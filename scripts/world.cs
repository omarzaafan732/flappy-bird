using Godot;
using System;

public class world : Node2D
{
    KinematicBody2D bird;
    KinematicBody2D pipe1;
    KinematicBody2D pipe2;
    KinematicBody2D pipe3;
    KinematicBody2D pipe4;
    Label label;
    Sprite scoureBoard;
    Label scoreLable;
    Label highScoreLable;

    public override void _Ready()
    {
        bird = GetNode<KinematicBody2D>("bird");
        pipe1 = GetNode<KinematicBody2D>("pipes/pipe");
        pipe2 = GetNode<KinematicBody2D>("pipes/pipe2");
        pipe3 = GetNode<KinematicBody2D>("pipes/pipe3");
        pipe4 = GetNode<KinematicBody2D>("pipes/pipe4");
        bird.Connect("restartGame", this, "onRestartGame");
        label = GetNode<Label>("UI/Label");
        scoureBoard = GetNode<Sprite>("UI/ScoreBoard");
        scoreLable = GetNode<Label>("UI/ScoreBoard/Score");
        highScoreLable = GetNode<Label>("UI/ScoreBoard/High score");
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        label.Text = globals.score.ToString();
        scoreLable.Text = globals.score.ToString();
        if (globals.gameState == 1)
        {
            label.Visible = true;
        }
        else
        {
            label.Visible = false;
        }

        if (globals.gameState == 2)
        {
            highScoreLable.Text = globals.HighScore.ToString();
            scoureBoard.Visible = true;
        }
        else
        {
            scoureBoard.Visible = false;
        }
    }

    public void onRestartGame()
    {
        globals.gameState = 0;
        pipe1.Call("restart");
        pipe2.Call("restart");
        pipe3.Call("restart");
        pipe4.Call("restart");
        bird.Call("restart");
        globals.score = 0;
    }
}
