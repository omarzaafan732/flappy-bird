using Godot;
using System;

public class bird : KinematicBody2D
{
    float gravity = 1500;
    Vector2 movingVector = new Vector2();
    float jumpPower = 500;
    AnimationPlayer animationPlayer;
    [Signal]
    delegate void restartGame();
    public override void _Ready()
    {
        animationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (globals.gameState != 0)
        {
            movingVector.y += gravity * delta;
            movingVector = MoveAndSlide(movingVector);
            rotateBird();
        }

        if(Input.IsActionJustPressed("jump"))
        {
            if (globals.gameState != 2)
            {
                movingVector.y = -jumpPower;
            }

            if (globals.gameState == 0)
            {
                globals.gameState = 1;
                animationPlayer.Play("flapping");
            }

            if (globals.gameState == 2)
            {
                EmitSignal("restartGame");
            }

        }
    }

    public void rotateBird()
    {
        if(movingVector.y < 300)
        {
            RotationDegrees = -15;
        }
        else
        {
            float clampedVelocity = Mathf.Clamp(movingVector.y, 300, 740) - 300;
            RotationDegrees = clampedVelocity / 6;
        }
    }

    public void _on_Area2D_area_entered(Area2D area)
    {
        globals.gameState = 2;
        animationPlayer.Stop();
    }

    public void restart()
    {
        GlobalPosition = new Vector2(GlobalPosition.x, 0);
        movingVector.y = 0;
        animationPlayer.Play("babbing");
        RotationDegrees = 0;
    }

    public void _on_pointArea_area_entered(Area2D area)
    {
        globals.score += 1;
        if (globals.score > globals.HighScore)
        {
            globals.HighScore = globals.score;
        }
    }
}
