using Godot;
using System;

public class globals : Node
{
    public static float scrollSpeed = 100;

    public static int score = 0;
    public static int HighScore = 0;
    public static int gameState = 0;
    //0 for pre-game
    //1 for playing
    //2 for Die
}
