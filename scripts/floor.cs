using Godot;
using System;

public class floor : KinematicBody2D
{
    float scrollSpeed = globals.scrollSpeed;
    public override void _Ready()
    {
        
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (globals.gameState != 2)
        {
            GlobalPosition -= new Vector2(scrollSpeed * delta, 0);
            if (GlobalPosition.x < -1024)
            {
                GlobalPosition += new Vector2(2048, 0);
            }
        }
    }
}
