using Godot;
using System;

public class pipe : KinematicBody2D
{
	float scrollSpeed = globals.scrollSpeed;
	Vector2 moveVector;
	int screenWidth = 1024;
	int pipeWidth = 96;
	RandomNumberGenerator rnd = new RandomNumberGenerator();
	float startingPoit;
	public override void _Ready()
	{
	   moveVector = new Vector2(-scrollSpeed, 0);
	   rnd.Randomize();
	   setPipeHights();
	   startingPoit = GlobalPosition.x;
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		if (globals.gameState == 1)
		{
			moveVector = MoveAndSlide(moveVector);
			if (GlobalPosition.x < -((screenWidth + pipeWidth) / 2))
			{
				GlobalPosition += new Vector2(screenWidth + pipeWidth, 0);
				setPipeHights();
			}
		}
	}

	public void setPipeHights()
	{
		float maxPipeHight = -170;
		float minPipeHght = 100;
		float randomHight = rnd.RandfRange(minPipeHght, maxPipeHight);
		GlobalPosition = new Vector2(GlobalPosition.x, randomHight);
	}

	public void restart()
	{
		GlobalPosition = new Vector2(startingPoit, GlobalPosition.y);
		setPipeHights();
	}
}
